Mentions légales - Dictionnaire des francophones 
================================================
1- Éditeur
-----------

Le site internet
[*www.dictionnairedesfrancophones.org*](http://www.dictionnairedesfrancophones.org)
ainsi que les applications mobiles « *Dictionnaire des francophones* »
sont édités par le ministère français de la Culture, 182 rue Saint
Honoré, 75 033 Paris Cedex 01, France.

2- Directeur de la publication
------------------------------

Le directeur de la publication est M. Paul de Sinety, Délégué général à
la langue française et aux langues de France au ministère français de la
Culture.

Les contenus proposés sur le *Dictionnaire des francophones* proviennent
des contributions des usagers et de différentes sources externes,
sélectionnées par le conseil scientifique du *Dictionnaire des
francophones*. Nous vous invitons à consulter la page [*Présentation du
projet*](http://www.dictionnairedesfrancophones.org/presentation) pour
davantage d'informations à ce sujet.

3- Hébergement
--------------

Le site internet
[*www.dictionnairedesfrancophones.org*](http://www.dictionnairedesfrancophones.org)
est hébergé par la très grande infrastructure de recherche
[*Huma-Num*](https://www.huma-num.fr/) - UMS 3598 CNRS, 54 boulevard
Raspail, 75006 Paris.

4- Réalisation du *Dictionnaire des francophones
------------------------------------------------

La plate-forme technique du *Dictionnaire des francophones* est
développée par la société [*Mnémotix*](https://www.mnemotix.com/), sous
le pilotage de l'Institut international pour la Francophonie (Université
Lyon III Jean Moulin).

Pour plus d'informations sur les partenaires et prestataires impliqués
dans la conception et la réalisation du *Dictionnaire des francophones*,
nous vous invitons à consulter la page
[*Crédits*](http://www.dictionnairedesfrancophones.org/credits).

5- Définitions
--------------

« *Dictionnaire des francophones* » désigne le site internet accessible
à l'adresse :
[*http://www.dictionnairedesfrancophones.org*](http://www.dictionnairedesfrancophones.org)
ainsi que les applications mobiles dénommées « *Dictionnaire des
francophones* » et disponibles sur les plates-formes Google Play et
Apple Store.

« Utilisateur » : désigne toute personne physique accédant au
*Dictionnaire des francophones* et/ou utilisant une des offres
éditoriales ou services qui y sont associés.

« Partenaire(s) » : désigne tout partenaire participant au projet du
*Dictionnaire des francophones*.

« Contenus » : désigne l'ensemble des données et plus généralement des
informations diffusées sur le *Dictionnaire des francophones*.

« Services » : désigne l'une ou l'ensemble des fonctionnalités fournies
sur le site et accessibles en ligne à partir du *Dictionnaire des
francophones*.
