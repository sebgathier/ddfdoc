Présentation du projet 
======================

Le *Dictionnaire des francophones* est un [dictionnaire](#dictionnaire) collaboratif numérique [ouvert](#ouvert) qui a pour objectif de rendre compte de la richesse du français parlé au sein de l'[espace francophone](#francophone). C'est un projet [institutionnel](#institutionnel) novateur qui présente à la fois une partie de consultation au sein de laquelle sont compilées plusieurs [ressources lexicographiques](#ressources), et une [partie participative](#participatif) pour développer [les mots](#mots) et faire vivre la [langue française](#langue).


# Dictionnaire <a id="dictionnaire"></a>

Le *Dictionnaire des francophones* est un ouvrage singulier. En effet, il se rapproche et s'inspire fortement des dictionnaires traditionnels, mais constitue un objet nouveau. Dans les dictionnaires imprimés traditionnels, les mots sont organisés par ordre alphabétique. Comme le DDF est une ressource en ligne, les données qu'il contient peuvent être constamment réorganisées en fonction des critères de recherche des utilisateurs. Par ailleurs, les utilisateurs sont encouragés à contribuer au dictionnaire en ajoutant du contenu et en validant les informations présentes. Les données du DDF sont donc appelées à s'enrichir et à se préciser de manière dynamique. 

Les dictionnaires généraux du français se focalisent sur le français standard, commun, par rapport auquel les variétés sociales (argot, mots familiers, etc.) ou géographiques (belgicismes, québécismes, etc.) sont situées. Le but du DDF est de décrire dans une même ressource tous les mots du français, quel que soit la région ou le registre de langue où ils sont utilisés, pour donner accès à une image instantanée, sans cesse renouvelée, du français dans le monde. Il ne s'agit donc pas de prescrire un français correct qui devrait être utilisé plutôt qu'un autre, c'est-à-dire une norme de langue, mais de rendre compte le plus précisément possible de l'usage pour permettre aux utilisateurs de choisir l'expression la plus adaptée à la situation.

# Un point sur les données ouvertes <a id="ouvert"></a>

Les données ouvertes ou *open data* sont des données numériques dont la structure est documentée ouvertement et qui sont encadrées par une licence garantissant leur réutilisation sans restriction technique, juridique ou financière. L'ouverture des données s'inscrit dans une tendance qui considère l'information comme un bien commun. La majeure partie des données du *Dictionnaire des francophones* sont publiées comme données ouvertes.

# Perspective francophone  <a id="francophone"></a>  
 
Dans le *Dictionnaire des francophones*, toutes les variétés du français sont présentées ensemble et placées sur un pied d'égalité. Chacune de ces variétés est une norme au sein de la région de la francophonie dans laquelle elle est parlée. 
 
Le terme francophonie, ou espace francophone, désigne l'ensemble des personnes et institutions qui utilisent le français que ce soit en tant que langue maternelle (première langue apprise par l'enfant), langue d'usage (standard, utilisée dans la vie de tous les jours), administrative, d'enseignement ou langue officielle (utilisée dans la vie publique, à chaque moment de la vie et dans chaque domaine). Est donc francophone toute personne ou institution utilisant le français, que ce soit partiellement ou majoritairement. 

Le nombre de ces francophones s'élève à plus de 300 millions de personnes dans le monde, dont environ 240 millions qui en font quotidiennement usage. Le français est devenu une langue-monde. Ces chiffres rendent compte de l'ampleur de la langue française sur les cinq continents. Rendre compte de la diversité des usages au sein de ce vaste ensemble n'est pas tâche aisée, c'est pourquoi le *Dictionnaire des francophones* compte sur l'apport des utilisateurs pour s'enrichir et proposer la plus grande palette d'information possible. 


# Origine du Dictionnaire des francophones <a id="institutionnel"></a>

Ce projet découle du discours du 20 mars 2018 du président de la République française, Emmanuel Macron. Un mandat a été confié à la Délégation générale à la langue française et aux langues de France (DGLFLF) et à un comité de pilotage interinstitutionnel qui a choisi comme opérateur l'Institut international pour la Francophonie (2IF), composante de l'université Jean Moulin Lyon 3. Un conseil scientifique a ensuite été réuni autour du projet, présidé par Bernard Cerquiglini.


# Présentation des ressources <a id="ressources"></a>
 
Le *Dictionnaire des francophones* a la particularité d'intégrer plusieurs ressources en son sein : le *Wiktionnaire* francophone, l'*Inventaire des particularités lexicales du français en Afrique noire*, le *Dictionnaire des synonymes, des mots et expressions des français parlés dans le monde*, *Le Grand Dictionnaire terminologique*, l'ouvrage *Belgicismes - Inventaire des particularités lexicales du français en Belgique*, le *Dictionnaire des régionalismes de France*, et la *Base de données lexicographiques panfrancophone*. *FranceTerme* est en cours d'intégration et rejoindra bientôt le *Dictionnaire des francophones* ! 

Le *Wiktionnaire* se définit lui-même comme un « projet lexicographique collaboratif accessible par internet hébergé par la Wikimedia Foundation, sous licence libre, visant à décrire dans toutes les langues tous les mots ». Il a débuté en mars 2004 et décrit en français plus de 420 000 mots de français. Le *Dictionnaire des francophones* intègre la partie francophone du projet, les mots de français décrits en français. C'est une ressource en constante évolution, qui est collaborative, c'est-à-dire rédigée, organisée et éditorialisée par des bénévoles, un fonctionnement qui se rapproche de celui du *Dictionnaire des francophones*. De nombreux principes sont communs entre les deux projets, qui se veulent complémentaires.

L'*Inventaire des particularités lexicales du français en Afrique noire* est un ouvrage qui fait la synthèse des lexiques et inventaires décrits pour onze pays d'Afrique francophones : Côte d'Ivoire, Togo, Bénin, République démocratique du Congo (Zaïre dans la publication originale), Tchad, Sénégal, Niger, Rwanda, Centrafrique, Cameroun et Burkina Faso. Il est le fruit du travail de plus de vingt linguistes, pendant plus de dix ans, de 1977 à sa publication en 1988. Grâce à l'Agence Universitaire de la Francophonie, il connait une nouvelle vie au sein du *Dictionnaire des francophones*. 

Le *Dictionnaire des synonymes, des mots et expressions des français parlés dans le monde* est une collecte de plus de 3 700 termes organisés par thèmes. Cette ressource est produite depuis 2013 par l'Académie des sciences d'Outre-mer et par l'Institut international pour la Francophonie - Université Jean Moulin Lyon III. Il a été publié en ligne en 2017 pour continuer à s'enrichir.

*Le Grand Dictionnaire terminologique* de l'Office québécois de la langue française a offert une sélection de ses entrées les plus intéressantes pour la Francophonie, grâce à un partenariat inédit. Cette banque de fiches terminologiques, dont la conception a débuté en 1974, présente des centaines de milliers de concepts liés à des domaines d'emploi spécialisés. Les concepts y sont définis et accompagnés des termes qui les désignent en français, en anglais et, parfois, dans d'autres langues. Plus de 4 000 entrées ont été intégrées au *Dictionnaire des francophones* grâce au soutien de l'Office québécois à la langue française.

Le dictionnaire *Belgicismes - Inventaire des particularités lexicales du français en Belgique* s'inscrit dans la continuité de l'*Inventaire* et ses auteurs participeront ensuite à la *Base de données lexicographiques panfrancophone*. Ce dictionnaire de belgicismes dresse l'inventaire des particularités lexicales du français en Belgique – dont certaines ne sont pas inconnues en France – avec leurs définitions, la localisation et, souvent, leur prononciation ainsi que des exemples d'emploi. Ce recueil a été rédigé par les sept membres belges du Conseil International de la Langue Française (CILF) et publié initialement en 1994. 

Le *Dictionnaire des régionalismes de France* est un ouvrage d'ensemble présentant une description attentive des régionalismes du français de France. Rédigé sous la direction de Pierre Rézeau, le DRF a réuni des chercheurs et des universitaires spécialistes de lexicographie, de géographie linguistique et d'histoire de la langue. Il a été publié en 2001. Il est intégré au *Dictionnaire des francophones* grâce au laboratoire de recherche Analyse et Traitement Informatique de la Langue Française (ATILF).

La *Base de données lexicographiques panfrancophone* est une œuvre collective débutée dans les années 1990 et publiée en ligne à partir de 2004. Elle réunit des études par une vingtaine de groupes de recherches différents concernant les régions ou pays suivants : Acadie, Algérie, Antilles, Belgique, Burundi, Cameroun, Centrafrique, Congo-Brazzaville, Côte d'Ivoire, France, Louisiane, Madagascar, Maroc, Maurice, Nouvelle-Calédonie, Québec, La Réunion, Rwanda, Suisse et Tchad. Elle décrit plus de 23 000 sens. Les entrées et les définitions de la *Base de données lexicographiques panfrancophone* sont intégrées dans le *Dictionnaire des francophones* grâce à un partenariat avec l'université de Laval. Les informations complémentaires, telles que les exemples et notes d'analyse peuvent être consultées sur leur site internet. 

*FranceTerme* est une base de données terminologiques créée et gérée par la Délégation générale de la langue française et aux langues de France du Ministère de la Culture. En ligne depuis mars 2008, elle compte plus de 7 000 termes récents avalisés par la Commission d'enrichissement de la langue française et parus au Journal officiel.


# Un dictionnaire participatif  <a id="participatif"></a>
 
Par participatif on entend un dictionnaire auquel chacun peut participer à sa manière en ajoutant des entrées, des exemples, en participant aux espaces de discussions, en signalant ou en validant des informations. Cette contribution est encadrée. La structure générale du *Dictionnaire des francophones* n'est pas modifiable. Des personnes reconnues par la communauté forment deux groupes d'utilisateurs dédiés à la relecture : les opérateurs et opératrices qui peuvent solliciter la suppression d'un contenu indésirable et les administrateurs et administratrices qui peuvent apporter des corrections ou supprimer des participations inappropriées.

La contribution ne peut se faire que par l'ajout d'information. C'est-à-dire qu'une information déjà présente ne peut être modifiée ou supprimée, ceci pour éviter des corrections abusives qui résulteraient de simples divergences d'opinions. Seuls les utilisateurs ayant le statut d'opérateurs ou d'opératrices sont habilités à supprimer des informations erronées.
    
Les espaces de discussion qui seront développés courant 2021 seront des zones d'échanges qui auront pour objectif d'apporter un supplément d'informations non directement présentes dans les articles du *Dictionnaire des francophones*. Il en existera trois : un espace de discussion pour l'étymologie, un espace réservé à la forme et un espace dédié au sens et à l'usage.
    
Que ce soit dans l'ajout d'information ou dans la discussion, il est possible de signaler des messages jugés abusifs afin de faire savoir aux opérateurs que cet article devrait être relu. Il est également possible de valider une information afin d'indiquer qu'elle est tout à fait correcte et intéressante, et ainsi la mettre en avant.

# Choix des entrées <a id="mots"></a>
 
Le *Dictionnaire des francophones* se veut représentatif de toute la diversité de la langue française. C'est pourquoi il présente déjà plus de 400 000 mots pour plus de 600 000 définitions. Et ce nombre va croitre grâce à l'aide de son lectorat actif qui deviendra contributorat. Tous les mots, tant qu'ils appartiennent à la langue française, peuvent y figurer. On trouvera donc des mots de nombreux pays francophones différents, des mots de la langue courante et des termes techniques, des insultes comme des compliments, des mots nouveaux aussi bien que vieux, et même des abus de langage ! Un même mot peut être écrit de diverses manières et chacune de ces formes pourra bénéficier d'une entrée distincte, dès lors qu'une existence est prouvée par des exemples d'usage.


# Précisions sur la langue  <a id="langue"></a>

Une langue est un bien commun dont les règles d'utilisation sont partagées par les utilisateurs et utilisatrices de cette langue.

La communauté linguistique du français est la francophonie. Elle recouvre de nombreux territoires de statuts divers : ville, régions, pays. L'unité administrative et l'unité langagière ne se recouvrent pas. Par exemple, le français est la langue nationale de la France, mais le territoire français est parsemé de bien d'autres communautés linguistiques pour des langues attachées à des régions comme le breton ou le basque, mais aussi des communautés dispersées parlant l'anglais, l'arabe, l'arménien, l'espagnol, le romani et bien d'autres langues liées aux flux humains.

Dans l'étude des usages des langues, il ne faut pas confondre les langues et les dialectes d'une langue, qui sont ses différentes variétés parlées au sein de la communauté linguistique. Les variétés partagent un grand vocabulaire commun ainsi que des traits grammaticaux avec les autres variétés de la langue. 

Les langues en contact avec le français l'influencent et certains de leurs termes entrent dans le langage courant. Par exemple, les mots français *bretzel* et *bugne* viennent respectivement de l'alsacien et du parler lyonnais, deux variétés du français qui sont influencés par deux langues : l'alémanique et le franco-provençal. Il est courant que les mots d'une langue soient proches ou similaires dans les autres langues utilisées dans la même région, et ces mots peuvent être décrits dans le *Dictionnaire des francophones*. Les mots qui sont spécifiques aux langues en contact et les règles de grammaire de ces langues ne sont pas décrits dans le DDF, car celui-ci ne décrit que le français.

Le nombre et les noms des variétés du français sont sujets à débats. Ils sont associés à des villes, des pays, à des régions historiques ou actuelles ou à de plus grands ensembles. Ils ne sont pas associés *a priori* aux définitions dans le DDF. 

Mentionnons également les créoles qui sont des langues à part entière parlées par un grand nombre de personnes partout dans le monde, notamment en Guyane, Louisiane et dans l'Océan Indien. Le terme « créole » est associé à une histoire des contacts de langue et associé aux influences qui ont conduit à leur création (créole à base lexicale anglaise, française, portugaise…). Les créoles à base lexicale française utilisent des mots issus de la langue française. Les langues qualifiées de créoles ne sont pas des variétés de français et ne figurent donc pas dans le *Dictionnaire des francophones*, qui est un dictionnaire monolingue. Cependant, certains mots de ces langues sont entrés dans la langue française et peuvent figurer dans le dictionnaire à ce titre. C'est notamment le cas des mots anse (plage de sable), boudin (plat au sang de cochon), cabri(t) (chèvre).

**Pour aller plus loin sur la nature de la langue :**

* Zevaco, Claudine. *Guide du français pour tous. Le livre de la francophonie*. L'Harmattan. 2000. 263 p. ISBN  : 2-7384-8452-2
* Organisation Internationale de la francophonie. *La langue française dans le monde*. Gallimard. 2019. 365 p. ISBN  : 978-2-07-278683-9
* Masseaut, Jean-Marc. *Creolization in the French Americas*. University of Louisiana. 2016. 286 p. p 139-231. ISBN  : 978-1-935754-68-8 
* Guenin-Lelle, Dianne. *The Story of French New Orleans. History of a Creole City*. University Press of Mississippi. 2016. 226 p. Chapter 3. ISBN : 978-1496804860
* Agora Francophone Internationale. *L'Année Francophone Internationale*. 1991-2019.
* Kilanga Musinde, Julien. *Langue française en Francophonie*. L'Harmattan. Collection “Savoirs”. 196 p. ISBN : 978-2-296-10754-0

# Historique des versions
L'idée de créer un dictionnaire francophone participatif est née en 2018. Le développement du *Dictionnaire des francophones* a débuté en mars 2019. Après plusieurs phases de tests sur des versions successives, le DDF a vu le jour en 2021. Pour assurer l'évolution constante du DDF, de nouvelles versions sont en cours de développement.

Voici l'évolution des versions : 
* 4 octobre 2019 : version interne de consultation mobile et bureau (desktop).
* 18 décembre 2019 : diffusion aux partenaires de la première version.
* 20 mars 2020 : diffusion d'une version permettant une première contribution.
* 20 mars 2021 : lancement au grand public d'une v. 1 incluant sept ressources et les fonctionnalités de base de la consultation.
