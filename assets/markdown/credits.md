Crédits
======================

<div align="center" style='margin-bottom:15px'>
	<img src="../images/partners/creditscollage.png" alt="partenaires" style="max-height:120px;max-width: 90vw;">
</div>

Projet majeur du plan d'action « [Une ambition pour la langue française et le plurilinguisme](https://www.elysee.fr/emmanuel-macron/2019/03/20/une-ambition-pour-la-langue-francaise-et-le-plurilinguisme) », lancé par le président de la République française en mars 2018, le *Dictionnaire des francophones* est né d'une première collaboration entre le ministère de la Culture français (Délégation générale à la langue française et aux langues de France) et l'Institut international pour la Francophonie, composante de l'Université Jean Moulin Lyon 3.

Le *Dictionnaire des francophones* est piloté par un consortium d'acteurs français et francophones issus du monde universitaire et de la recherche ainsi que de représentants de l'administration.

Le *Dictionnaire des francophones* est soutenu par le ministère de la Culture français, l'Agence universitaire de la Francophonie, l'Organisation internationale de la Francophonie et la très grande infrastructure de recherche Huma-Num-CNRS.

La direction opérationnelle du *Dictionnaire des francophones* est assurée par l'Institut international pour la Francophonie (Université Jean Moulin Lyon 3).

Les orientations techniques et scientifiques du *Dictionnaire des francophones* sont déterminées par son conseil scientifique qui est composé de : Bernard Cerquiglini, Paul de Sinety, Mathieu Avanzi, Olivier Baude, Claudine Bavoux, Aïcha Bouhjar, Barbara Cassin, Moussa Daff, Kaja Dolar, Ginette Galarneau, Pierre Geny, François Grin, Katia Haddad, Latifa Kadi-Ksouri, Jean-Marie Klinkenberg, Jérémie Kouadio, Mona Laroussi, Gabrielle Le Tallec Lloret, Lucas Lévêque, Franck Neveu, Marie Steffens, Jean Tabi Manga.


<div align="center" style='margin-bottom:15px;margin-top:15px'>
<img src="../images/partners/mnemotix.png" alt="Mnémotix" style="max-width:300px;margin:auto">
</div>


Le développement de la base de connaissance et du site internet du *Dictionnaire des francophones* a été réalisé par la société d'intérêt collectif [Mnémotix](https://www.mnemotix.com/), suite à la publication d'un marché public. 


* Chef de projet : Noé Gasparini
* Responsable opérationnel : Sébastien Gathier
* Responsable des communautés : Nadia Sefiane
* Équipe de conception : Noé Gasparini, Kaja Dolar, Marie Steffens, Antoine Bouchez
* Conception de l'ontologie : Jean Delahousse et Noé Gasparini
* Création du logo et de la charte graphique : Alexandra Simon
* Design UX : Jérémie Cohen
* Numérisation et intégration de ressources : Laurent Catach (4H Conseil) 
* Équipe de développement : Mnémotix  (Freddy Limpens, Nicolas Delaforge, Mylène Leitzelman, Pierre-René Lherisson, Mathieu Rogelja, Quentin Richaud, Alain Ibrahim) 
* Équipe de développement des applications mobiles : Brave Cactus (Alison Andrault, Olivier Bernal, Benjamin Hamon, Cindy Hamon, Sophie Huart)
* Conception des interfaces : Makewaves et Atelier Jugeote (Antoine Roquis, Manon Verbeke, Melvin Buseniers, Raphaelle Penhoud, Anaëlle Razafimamonjiarison) 
* Équipe à l'Institut international pour la Francophonie : Thomas Meszaros, Sandrine David, Noé Gasparini, Sébastien Gathier, Nadia Sefiane, Aurore Sudre, Hong Khanh Dang, Camelia Danc, Léa Bugin, Lucas Prégaldiny et Louisa Curci
* Hébergement : Huma-Num, infrastructure CNRS
