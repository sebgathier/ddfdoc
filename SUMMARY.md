# Summary

* [Introduction](README.md)
* [Présentation du projet](assets/markdown/project_presentation.md)
* [Mentions légales](assets/markdown/gcu.md)
* [Aide et documentation](README.aide.md)
    * [Recherche et compte utilisateur](assets/markdown/help_how_to_search.md)
    * [Comprendre un article de dictionnaire](assets/markdown/help_how_to_read.md)
* [Visualisation de données](dataviz.md)
* [Partenaires du projet](assets/markdown/partners.md)